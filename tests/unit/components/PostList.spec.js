import { shallowMount } from '@vue/test-utils'

import PostList from '@/components/PostList'
import PostListRow from '@/components/PostListRow'
import postsFixture from '../fixtures/posts'

describe('PostList', () => {
  let props

  const build = () => {
    const wrapper = shallowMount(PostList, {
      propsData: props
    })
    return {
      wrapper,
      list: () => wrapper.find('ul'),
      items: () => wrapper.findAll(PostListRow)
    }
  }

  beforeEach(() => {
    props = {
      posts: postsFixture
    }
  })

  it('renders the component', () => {
    const { wrapper } = build()
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('renders the main child components', () => {
    const { list } = build()
    expect(list().exists()).toBe(true)
  })

  it('renders one row per post and pass the post prop', () => {
    const { items } = build()
    expect(items()).toHaveLength(props.posts.length)
    for (let i = 0; i < items().length; i++) {
      expect(items().at(i).vm.post).toBe(props.posts[i])
    }
  })
})
