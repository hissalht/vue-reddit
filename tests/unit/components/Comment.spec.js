import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'

import Comment from '@/components/Comment'
import commentsFixture from '../fixtures/normalizedComments'
import storeFixture from '../fixtures/store'
import * as actions from '@/store/actions'

const localVue = createLocalVue()
localVue.use(Vuex)

jest.mock('@/store/actions')

describe('Comment', () => {
  let props
  let state

  const build = () => {
    const wrapper = shallowMount(Comment, {
      localVue,
      propsData: props,
      store: new Vuex.Store({ state, actions })
    })
    return {
      wrapper,
      children: () => wrapper.findAll(Comment),
      body: () => wrapper.find('.content'),
      header: () => wrapper.find('.header')
    }
  }

  beforeEach(() => {
    jest.resetAllMocks()
    props = {
      comment: commentsFixture[0]
    }
    state = storeFixture
  })

  it('renders the component', () => {
    const { wrapper } = build()
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('accepts the correct props', () => {
    const { wrapper } = build()
    expect(wrapper.vm.comment).toBeDefined()
  })

  it('binds children comments from the store', () => {
    const { wrapper } = build()
    expect(wrapper.vm.childComments).toHaveLength(2)
  })

  it('renders a Comment for each child comment', () => {
    const { children } = build()
    expect(children().length).toEqual(2 + 1) // 2 children + the root Comment
  })

  it('renders the body of the comment', () => {
    const { body } = build()
    expect(body().exists()).toBe(true)
    expect(body().element.innerHTML).toEqual(
      props.comment.bodyHtml
    )
  })

  it('renders the author of the comment', () => {
    const { header } = build()
    expect(header().exists()).toBe(true)
    expect(header().text()).toContain(props.comment.author)
  })

  it('doesnt render children and content if comment.hidden is true', () => {
    props.comment.hidden = true
    const { body, children } = build()
    expect(body().exists()).toBe(false)
    expect(children()).toHaveLength(1)
  })

  it('dispatch TOGGLE_HIDDEN_COMMENT on click', () => {
    const { wrapper } = build()
    wrapper.trigger('click.stop')
    expect(actions.TOGGLE_HIDDEN_COMMENT).toHaveBeenCalled()
    expect(actions.TOGGLE_HIDDEN_COMMENT.mock.calls[0][1]).toEqual({ id: commentsFixture[0].id })
  })
})
