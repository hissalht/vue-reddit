import { shallowMount } from '@vue/test-utils'

import SubFinder from '@/components/SubFinder'

describe('SubFinder', () => {
  let push

  const build = () => {
    const wrapper = shallowMount(SubFinder, {
      mocks: {
        $router: {
          push
        }
      }
    })
    return {
      wrapper,
      input: () => wrapper.find('input'),
      button: () => wrapper.find('button')
    }
  }

  beforeEach(() => {
    push = jest.fn()
  })

  it('renders the component', () => {
    const { wrapper } = build()
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('renders an form with input and submit button', () => {
    const { wrapper, input, button } = build()
    expect(wrapper.is('form')).toBe(true)
    expect(input().exists()).toBe(true)
    expect(button().exists()).toBe(true)
    expect(button().element.getAttribute('type')).toEqual('submit')
  })

  it('navigates to the required sub frontpage on form submit', () => {
    const sub = 'vuejs'
    const { wrapper, input } = build()
    input().setValue(sub)
    wrapper.trigger('submit')
    expect(push).toHaveBeenCalledWith({ name: 'posts', params: { sub } })
  })

  it('doesnt navigate if input value is empty', () => {
    const sub = '   '
    const { wrapper, input } = build()
    input().setValue(sub)
    wrapper.trigger('submit')
    expect(push).not.toHaveBeenCalled()
  })
})
