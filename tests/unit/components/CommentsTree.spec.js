import { shallowMount } from '@vue/test-utils'

import CommentsTree from '@/components/CommentsTree'
import Comment from '@/components/Comment'
import commentsFixture from '../fixtures/normalizedComments'

describe('CommentTree', () => {
  let props

  const build = () => {
    const wrapper = shallowMount(CommentsTree, {
      propsData: props
    })
    return {
      wrapper,
      children: () => wrapper.findAll(Comment)
    }
  }

  beforeEach(() => {
    props = {
      comments: commentsFixture.slice(0, 2)
    }
  })

  it('renders the component', () => {
    const { wrapper } = build()
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('accepts a comments prop', () => {
    const { wrapper } = build()
    expect(wrapper.vm.comments).toBeDefined()
  })

  it('renders a Comment for each items in comments prop', () => {
    const { children } = build()
    expect(children()).toHaveLength(props.comments.length)
    for (let i = 0; i < props.comments.length; i++) {
      expect(children().at(i).vm.comment).toBe(props.comments[i])
    }
  })
})
