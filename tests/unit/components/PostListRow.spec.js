import { shallowMount, createLocalVue, RouterLinkStub } from '@vue/test-utils'
import VueRouter from 'vue-router'

import PostListRow from '@/components/PostListRow'
import postsFixture from '../fixtures/normalizedPosts'
import routes from '@/router/routes'

const postFixture = postsFixture[0]

const localVue = createLocalVue()
localVue.use(VueRouter)
const router = new VueRouter(routes)

describe('PostListRow', () => {
  let props

  const build = () => {
    const wrapper = shallowMount(PostListRow, {
      propsData: props,
      localVue,
      router,
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
    return {
      wrapper,
      link: () => wrapper.find(RouterLinkStub),
      score: () => wrapper.find('.score')
    }
  }

  beforeEach(() => {
    props = {
      post: postFixture
    }
  })

  it('renders the component', () => {
    const { wrapper } = build()
    expect(wrapper.is('li')).toBe(true)
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('renders the main children components', () => {
    const { link, score } = build()

    expect(link().exists()).toBe(true)
    expect(link().vm.to).toEqual({
      name: 'comments',
      params: {
        sub: postFixture.subreddit,
        id: postFixture.id.slice(3), // remove the 't3_' part of the id
        slug: 'slug'
      }
    })

    expect(score().exists()).toBe(true)
    expect(score().text()).toEqual('' + postFixture.score)
  })

  it('accepts a post prop', () => {
    const { wrapper } = build()
    expect(wrapper.vm.post).toBe(postFixture)
  })

  it('renders the post title', () => {
    const { wrapper } = build()
    expect(wrapper.text()).toContain(postFixture.title)
  })
})
