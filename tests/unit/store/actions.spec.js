import * as actions from '@/store/actions'
import flushPromises from 'flush-promises'

import postsFixture from '../fixtures/normalizedPosts'
import commentsFixture from '../fixtures/normalizedComments'

jest.mock('@/api')

describe('actions', () => {
  let commit

  beforeEach(() => {
    commit = jest.fn()
  })

  describe('FETCH_LINKS', () => {
    it('commits SET_LINKS and SET_AFTER', async () => {
      const sub = 'vuejs'
      await actions.FETCH_LINKS({ commit }, { sub })
      await flushPromises()
      expect(commit).toHaveBeenCalledWith('SET_LINKS', { links: postsFixture })
      expect(commit).toHaveBeenCalledWith('SET_AFTER', { sub, after: 't3_mocked' })
    })

    it('handles the invalidate payload parameter', async () => {
      const sub = 'vuejs'
      await actions.FETCH_LINKS({ commit }, { sub, invalidate: true })
      await flushPromises()
      expect(commit).toHaveBeenCalledWith('SET_LINKS', { links: postsFixture, invalidate: true })
    })
  })

  it('FETCH_COMMENTS', async () => {
    const sub = 'vuejs'
    const linkId = 't3_abcdef'
    await actions.FETCH_COMMENTS({ commit }, { sub, link: linkId })
    await flushPromises()
    expect(commit).toHaveBeenCalledWith('SET_COMMENTS', { comments: commentsFixture })
    expect(commit).toHaveBeenCalledWith('SET_LINKS', { links: [postsFixture[0]] })
  })

  describe('TOGGLE_HIDDEN_COMMENT', () => {
    it('commits HIDE_COMMENT if the hidden property is falsy', () => {
      const linkId = 't1_abcdef'
      const state = {
        comments: {
          entities: {
            [linkId]: { }
          }
        }
      }
      actions.TOGGLE_HIDDEN_COMMENT({ commit, state }, { id: linkId })
      expect(commit).toHaveBeenCalledWith('HIDE_COMMENT', { id: linkId })
    })
    it('commits SHOW_COMMENT if the hidden property is true', () => {
      const linkId = 't1_abcdef'
      const state = {
        comments: {
          entities: {
            [linkId]: { hidden: true }
          }
        }
      }
      actions.TOGGLE_HIDDEN_COMMENT({ commit, state }, { id: linkId })
      expect(commit).toHaveBeenCalledWith('SHOW_COMMENT', { id: linkId })
    })
  })
})
