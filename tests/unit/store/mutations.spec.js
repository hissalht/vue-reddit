import { getInitialState } from '@/store/state'
import * as mutations from '@/store/mutations'
import postsFixture from '../fixtures/normalizedPosts'
import normalizedComments from '../fixtures/normalizedComments'
import storeFixture from '../fixtures/store'

describe('mutations', () => {
  let state

  beforeEach(() => {
    state = getInitialState()
  })

  describe('SET_LINKS', () => {
    it('puts the links in the store', () => {
      const links = postsFixture
      mutations.SET_LINKS(state, { links })
      for (let post of postsFixture) {
        expect(state.links.entities[post.id]).toEqual(post)
        expect(state.links.entities[post.id]).not.toBe(post)
        expect(state.links.subredditIndex[post.subreddit]).toContain(post.id)
      }
    })

    it('generate a new subredditIndex when invalidate is true', () => {
      const links = postsFixture
      mutations.SET_LINKS(state, { links })
      expect(state.links.subredditIndex['gifs'].length).toBeGreaterThan(0)
      mutations.SET_LINKS(state, { links: [], invalidate: true })
      expect(state.links.subredditIndex['gifs']).toBeUndefined()
    })

    it('doesnt push ids to subredditIndex if already in there', () => {
      const links = postsFixture
      mutations.SET_LINKS(state, { links })
      const l = state.links.subredditIndex.gifs.length
      mutations.SET_LINKS(state, { links })
      expect(state.links.subredditIndex.gifs).toHaveLength(l)
    })
  })

  describe('SET_AFTER', () => {
    it('sets the after id in the store', () => {
      const after = 't3_abcdef'
      const sub = 'vuejs'
      mutations.SET_AFTER(state, { sub, after })
      expect(state.links.afters[sub]).toEqual(after)
    })
  })

  describe('SET_COMMENTS', () => {
    it('puts the comments in the store', () => {
      mutations.SET_COMMENTS(state, { comments: normalizedComments })
      for (let comment of normalizedComments) {
        expect(state.comments.entities[comment.id]).toEqual(comment)
        expect(state.comments.entities[comment.id]).not.toBe(comment)
        if (comment.depth === 0) {
          expect(state.comments.linkIndex[comment.link]).toContain(comment.id)
        } else {
          expect(state.comments.linkIndex[comment.link]).not.toContain(comment.id)
        }
      }
    })

    it('generate a new linkIndex when invalidate is true', () => {
      mutations.SET_COMMENTS(state, { comments: normalizedComments })
      expect(state.comments.linkIndex['t3_linkab'].length).toBeGreaterThan(0)
      mutations.SET_COMMENTS(state, { comments: [], invalidate: true })
      expect(state.comments.linkIndex['t3_linkab']).toBeUndefined()
    })

    it('doesnt push comment ids to linkIndex if already in there', () => {
      mutations.SET_COMMENTS(state, { comments: normalizedComments })
      const l = state.comments.linkIndex.t3_linkab.length
      mutations.SET_COMMENTS(state, { comments: normalizedComments })
      expect(state.comments.linkIndex.t3_linkab.length).toEqual(l)
    })
  })

  describe('HIDE_COMMENT', () => {
    it('set the hidden property of a comment to true', () => {
      const commentId = 't1_aaaaaa'
      state = JSON.parse(JSON.stringify(storeFixture))
      mutations.HIDE_COMMENT(state, { id: commentId })
      expect(state.comments.entities[commentId].hidden).toBe(true)
    })
  })

  describe('SHOW_COMMENT', () => {
    it('set the hidden property of a comment to false', () => {
      const commentId = 't1_aaaaaa'
      state = JSON.parse(JSON.stringify(storeFixture))
      state.comments.entities[commentId].hidden = true
      mutations.SHOW_COMMENT(state, { id: commentId })
      expect(state.comments.entities[commentId].hidden).toBe(false)
    })
  })
})
