import posts from './normalizedPosts'
import comments from './normalizedComments'
import { getInitialState } from '@/store/state'

export default {
  ...getInitialState(),
  comments: {
    entities: {
      [comments[0].id]: comments[0],
      [comments[1].id]: comments[1],
      [comments[2].id]: comments[2],
      [comments[3].id]: comments[3],
      [comments[4].id]: comments[4]
    }, // fullname => comment
    linkIndex: {
      't3_linkab': [comments[0].id, comments[1].id],
      't3_linkxx': [comments[1].id]
    }
  },
  links: {
    entities: {
      [posts[0].id]: posts[0],
      [posts[1].id]: posts[1],
      [posts[0].id + 'a']: posts[0],
      [posts[1].id + 'a']: posts[1],
      [posts[2].id]: posts[2]
    }, // fullname => link
    subredditIndex: {
      'gifs': [posts[0].id, posts[0].id + 'a'],
      'pics': [posts[1].id, posts[1].id + 'a'],
      'vuejs': [posts[2].id]

    }, // subreddit => [linkIds]
    afters: {} // sub => after
  }
}
