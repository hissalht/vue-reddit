export default [
  {
    id: 't1_aaaaaa',
    body: 'Foo bar',
    bodyHtml: '<p>Foo bar</p>',
    score: 100,
    link: 't3_linkab',
    author: 'Bob',
    depth: 0,
    replies: ['t1_aaabbb', 't1_aaaccc']
  },
  {
    id: 't1_bbbbbb',
    body: 'Hello world',
    bodyHtml: '<p>Hello world</p>',
    score: 203,
    link: 't3_linkab',
    author: 'Marie',
    depth: 0,
    replies: []
  },
  {
    id: 't1_cccccc',
    body: 'I like chocolate',
    bodyHtml: '<p>I like chocolate</p>',
    score: 300,
    link: 't3_linkxx',
    author: 'Bob',
    depth: 0,
    replies: []
  },
  {
    id: 't1_aaabbb',
    body: 'I like chocolate',
    bodyHtml: '<p>I like chocolate</p>',
    score: 300,
    link: 't3_linkab',
    author: 'Bob',
    depth: 1,
    replies: []
  },
  {
    id: 't1_aaaccc',
    body: 'I like chocolate',
    bodyHtml: '<p>I like chocolate</p>',
    score: 300,
    link: 't3_linkab',
    author: 'Bob',
    depth: 1,
    replies: []
  }
]
