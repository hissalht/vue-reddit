export default [
  {
    'approved_at_utc': null,
    'subreddit': 'gifs',
    'selftext': '',
    'author_fullname': 't2_gkb30',
    'saved': false,
    'mod_reason_title': null,
    'gilded': 1,
    'clicked': false,
    'title': 'Hong Kong Protesters helping Journalist after tear gas deployment',
    'link_flair_richtext': [

    ],
    'subreddit_name_prefixed': 'r/gifs',
    'hidden': false,
    'pwls': 6,
    'link_flair_css_class': null,
    'downs': 0,
    'thumbnail_height': 78,
    'hide_score': false,
    'name': 't3_c0357x',
    'quarantine': false,
    'link_flair_text_color': 'dark',
    'author_flair_background_color': null,
    'subreddit_type': 'public',
    'ups': 7588,
    'total_awards_received': 1,
    'media_embed': {

    },
    'thumbnail_width': 140,
    'author_flair_template_id': null,
    'is_original_content': false,
    'user_reports': [

    ],
    'secure_media': null,
    'is_reddit_media_domain': true,
    'is_meta': false,
    'category': null,
    'secure_media_embed': {

    },
    'link_flair_text': null,
    'can_mod_post': false,
    'score': 7588,
    'approved_by': null,
    'thumbnail': 'https://b.thumbs.redditmedia.com/yvwTVTDkchIeEHoHrO6vpGbak8G9YAXJPdCso1smARY.jpg',
    'edited': false,
    'author_flair_css_class': null,
    'author_flair_richtext': [

    ],
    'gildings': {
      'gid_2': 1
    },
    'post_hint': 'image',
    'content_categories': null,
    'is_self': false,
    'mod_note': null,
    'created': 1560442621.0,
    'link_flair_type': 'text',
    'wls': 6,
    'banned_by': null,
    'author_flair_type': 'text',
    'domain': 'i.redd.it',
    'selftext_html': null,
    'likes': null,
    'suggested_sort': null,
    'banned_at_utc': null,
    'view_count': null,
    'archived': false,
    'no_follow': false,
    'is_crosspostable': false,
    'pinned': false,
    'over_18': false,
    'preview': {
      'images': [
        {
          'source': {
            'url': 'https://preview.redd.it/8cl1whsq13431.gif?format=png8&amp;s=97b78fe6efe06eff964ce23a110f561baebab688',
            'width': 400,
            'height': 225
          },
          'resolutions': [
            {
              'url': 'https://preview.redd.it/8cl1whsq13431.gif?width=108&amp;crop=smart&amp;format=png8&amp;s=b13120aea479e7b6ce13f7dbd3b6396ac3119fb3',
              'width': 108,
              'height': 60
            },
            {
              'url': 'https://preview.redd.it/8cl1whsq13431.gif?width=216&amp;crop=smart&amp;format=png8&amp;s=d1604dd75040d0de158f687750dd138cc00c4b0a',
              'width': 216,
              'height': 121
            },
            {
              'url': 'https://preview.redd.it/8cl1whsq13431.gif?width=320&amp;crop=smart&amp;format=png8&amp;s=ef22c95a7455309548de0768fef680624ba66567',
              'width': 320,
              'height': 180
            }
          ],
          'variants': {
            'gif': {
              'source': {
                'url': 'https://preview.redd.it/8cl1whsq13431.gif?s=e59dab7d47ac4a324c6e715ae531156b6e06b198',
                'width': 400,
                'height': 225
              },
              'resolutions': [
                {
                  'url': 'https://preview.redd.it/8cl1whsq13431.gif?width=108&amp;crop=smart&amp;s=8fe3ec4e133e3dffd19e60ddc1c3f5c54daee5b7',
                  'width': 108,
                  'height': 60
                },
                {
                  'url': 'https://preview.redd.it/8cl1whsq13431.gif?width=216&amp;crop=smart&amp;s=02fc2206bc81ff4ee84ea8751be7468d50252a6b',
                  'width': 216,
                  'height': 121
                },
                {
                  'url': 'https://preview.redd.it/8cl1whsq13431.gif?width=320&amp;crop=smart&amp;s=7091db6dc552bfdc0716a341ad4ae729ec77c3a1',
                  'width': 320,
                  'height': 180
                }
              ]
            },
            'mp4': {
              'source': {
                'url': 'https://preview.redd.it/8cl1whsq13431.gif?format=mp4&amp;s=a171dab8c7eb832336d66cfacde073cb612e525d',
                'width': 400,
                'height': 225
              },
              'resolutions': [
                {
                  'url': 'https://preview.redd.it/8cl1whsq13431.gif?width=108&amp;format=mp4&amp;s=e7c3a6c3b4c4c3663c08079ab8529325e3348083',
                  'width': 108,
                  'height': 60
                },
                {
                  'url': 'https://preview.redd.it/8cl1whsq13431.gif?width=216&amp;format=mp4&amp;s=e7806491d582b8dc121b9f1d0399b5453dbcead8',
                  'width': 216,
                  'height': 121
                },
                {
                  'url': 'https://preview.redd.it/8cl1whsq13431.gif?width=320&amp;format=mp4&amp;s=c31a9c222022091d4f93e1305d53cf2075a0a3fc',
                  'width': 320,
                  'height': 180
                }
              ]
            }
          },
          'id': 'L678_A3bFLtuUOmKqwRtqAwlYlD88xVEKnURyXCmc7Y'
        }
      ],
      'enabled': true
    },
    'all_awardings': [
      {
        'is_enabled': true,
        'count': 1,
        'subreddit_id': null,
        'description': 'Gold Award',
        'coin_reward': 100,
        'icon_width': 512,
        'icon_url': 'https://www.redditstatic.com/gold/awards/icon/gold_512.png',
        'days_of_premium': 7,
        'icon_height': 512,
        'resized_icons': [
          {
            'url': 'https://www.redditstatic.com/gold/awards/icon/gold_16.png',
            'width': 16,
            'height': 16
          },
          {
            'url': 'https://www.redditstatic.com/gold/awards/icon/gold_32.png',
            'width': 32,
            'height': 32
          },
          {
            'url': 'https://www.redditstatic.com/gold/awards/icon/gold_48.png',
            'width': 48,
            'height': 48
          },
          {
            'url': 'https://www.redditstatic.com/gold/awards/icon/gold_64.png',
            'width': 64,
            'height': 64
          },
          {
            'url': 'https://www.redditstatic.com/gold/awards/icon/gold_128.png',
            'width': 128,
            'height': 128
          }
        ],
        'days_of_drip_extension': 0,
        'award_type': 'global',
        'coin_price': 500,
        'id': 'gid_2',
        'name': 'Gold'
      }
    ],
    'media_only': false,
    'can_gild': false,
    'spoiler': false,
    'locked': false,
    'author_flair_text': null,
    'visited': false,
    'num_reports': null,
    'distinguished': null,
    'subreddit_id': 't5_2qt55',
    'mod_reason_by': null,
    'removal_reason': null,
    'link_flair_background_color': '',
    'id': 'c0357x',
    'is_robot_indexable': true,
    'report_reasons': null,
    'author': 'tomyyat',
    'num_crossposts': 3,
    'num_comments': 201,
    'send_replies': true,
    'whitelist_status': 'all_ads',
    'contest_mode': false,
    'mod_reports': [

    ],
    'author_patreon_flair': false,
    'author_flair_text_color': null,
    'permalink': '/r/gifs/comments/c0357x/hong_kong_protesters_helping_journalist_after/',
    'parent_whitelist_status': 'all_ads',
    'stickied': false,
    'url': 'https://i.redd.it/8cl1whsq13431.gif',
    'subreddit_subscribers': 18573640,
    'created_utc': 1560413821.0,
    'media': null,
    'is_video': false
  },
  {
    'approved_at_utc': null,
    'subreddit': 'pics',
    'selftext': '',
    'author_fullname': 't2_2hmw0kze',
    'saved': false,
    'mod_reason_title': null,
    'gilded': 0,
    'clicked': false,
    'title': 'Glass house',
    'link_flair_richtext': [

    ],
    'subreddit_name_prefixed': 'r/pics',
    'hidden': false,
    'pwls': 6,
    'link_flair_css_class': null,
    'downs': 0,
    'thumbnail_height': 140,
    'hide_score': false,
    'name': 't3_c012zb',
    'quarantine': false,
    'link_flair_text_color': 'dark',
    'author_flair_background_color': null,
    'subreddit_type': 'public',
    'ups': 26763,
    'total_awards_received': 0,
    'media_embed': {

    },
    'thumbnail_width': 140,
    'author_flair_template_id': null,
    'is_original_content': false,
    'user_reports': [

    ],
    'secure_media': null,
    'is_reddit_media_domain': true,
    'is_meta': false,
    'category': null,
    'secure_media_embed': {

    },
    'link_flair_text': null,
    'can_mod_post': false,
    'score': 26763,
    'approved_by': null,
    'thumbnail': 'https://b.thumbs.redditmedia.com/VjOrtZ64UF0tZgwxEUQohUcn4lWbdutq9Agvil3NPDc.jpg',
    'edited': false,
    'author_flair_css_class': null,
    'author_flair_richtext': [

    ],
    'gildings': {

    },
    'post_hint': 'image',
    'content_categories': [
      'photography'
    ],
    'is_self': false,
    'mod_note': null,
    'created': 1560427303.0,
    'link_flair_type': 'text',
    'wls': 6,
    'banned_by': null,
    'author_flair_type': 'text',
    'domain': 'i.redd.it',
    'selftext_html': null,
    'likes': null,
    'suggested_sort': null,
    'banned_at_utc': null,
    'view_count': null,
    'archived': false,
    'no_follow': false,
    'is_crosspostable': false,
    'pinned': false,
    'over_18': false,
    'preview': {
      'images': [
        {
          'source': {
            'url': 'https://preview.redd.it/uwdwiwxcs1431.jpg?auto=webp&amp;s=23710d090869ca182a3121ec13492ad8e5ab5255',
            'width': 1080,
            'height': 1350
          },
          'resolutions': [
            {
              'url': 'https://preview.redd.it/uwdwiwxcs1431.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=0f006d16423fda544b72ab3318540318c8814078',
              'width': 108,
              'height': 135
            },
            {
              'url': 'https://preview.redd.it/uwdwiwxcs1431.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=90fb7e126c2245fff34aa77b30558edaeb1a36e2',
              'width': 216,
              'height': 270
            },
            {
              'url': 'https://preview.redd.it/uwdwiwxcs1431.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=a811afb7dc2c7fc6202ab030126f6172dfca2eda',
              'width': 320,
              'height': 400
            },
            {
              'url': 'https://preview.redd.it/uwdwiwxcs1431.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=a002c7950c7c61b80b11d281daf46fbd9e7cc584',
              'width': 640,
              'height': 800
            },
            {
              'url': 'https://preview.redd.it/uwdwiwxcs1431.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=12e4816c679667230041a67a29e59a71429c17ba',
              'width': 960,
              'height': 1200
            },
            {
              'url': 'https://preview.redd.it/uwdwiwxcs1431.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=11a6383c29de2758a51e17d1fa7cba7bea0f7b73',
              'width': 1080,
              'height': 1350
            }
          ],
          'variants': {

          },
          'id': 'AvgP24BUv68d8vRB26G7vP8tMwDVr5FW1Q3Aip_sI-s'
        }
      ],
      'enabled': true
    },
    'all_awardings': [

    ],
    'media_only': false,
    'can_gild': false,
    'spoiler': false,
    'locked': false,
    'author_flair_text': null,
    'visited': false,
    'num_reports': null,
    'distinguished': null,
    'subreddit_id': 't5_2qh0u',
    'mod_reason_by': null,
    'removal_reason': null,
    'link_flair_background_color': '',
    'id': 'c012zb',
    'is_robot_indexable': true,
    'report_reasons': null,
    'author': 'CapSkittle1',
    'num_crossposts': 10,
    'num_comments': 462,
    'send_replies': true,
    'whitelist_status': 'all_ads',
    'contest_mode': false,
    'mod_reports': [

    ],
    'author_patreon_flair': false,
    'author_flair_text_color': null,
    'permalink': '/r/pics/comments/c012zb/glass_house/',
    'parent_whitelist_status': 'all_ads',
    'stickied': false,
    'url': 'https://i.redd.it/uwdwiwxcs1431.jpg',
    'subreddit_subscribers': 22069229,
    'created_utc': 1560398503.0,
    'media': null,
    'is_video': false
  },
  {
    'approved_at_utc': null,
    'subreddit': 'vuejs',
    'selftext': 'Should I make requests directly from my components or is it better practice to be using an action to call API requests from the store?',
    'user_reports': [

    ],
    'saved': false,
    'mod_reason_title': null,
    'gilded': 0,
    'clicked': false,
    'title': 'MEVN stack App: Should I be making API requests from my vuex store?',
    'link_flair_richtext': [

    ],
    'subreddit_name_prefixed': 'r/vuejs',
    'hidden': false,
    'pwls': null,
    'link_flair_css_class': null,
    'downs': 0,
    'thumbnail_height': null,
    'parent_whitelist_status': null,
    'hide_score': false,
    'name': 't3_c1yuig',
    'quarantine': false,
    'link_flair_text_color': 'dark',
    'upvote_ratio': 0.87,
    'author_flair_background_color': null,
    'subreddit_type': 'public',
    'ups': 10,
    'total_awards_received': 0,
    'media_embed': {

    },
    'thumbnail_width': null,
    'author_flair_template_id': null,
    'is_original_content': false,
    'author_fullname': 't2_m9j6u',
    'secure_media': null,
    'is_reddit_media_domain': false,
    'is_meta': false,
    'category': null,
    'secure_media_embed': {

    },
    'link_flair_text': null,
    'can_mod_post': false,
    'score': 10,
    'approved_by': null,
    'thumbnail': 'self',
    'edited': 1560847191.0,
    'author_flair_css_class': null,
    'author_flair_richtext': [

    ],
    'gildings': {

    },
    'content_categories': null,
    'is_self': true,
    'mod_note': null,
    'created': 1560868773.0,
    'link_flair_type': 'text',
    'wls': null,
    'banned_by': null,
    'author_flair_type': 'text',
    'domain': 'self.vuejs',
    'selftext_html': '&lt;!-- SC_OFF --&gt;&lt;div class="md"&gt;&lt;p&gt;Should I make requests directly from my components or is it better practice to be using an action to call API requests from the store?&lt;/p&gt;\n&lt;/div&gt;&lt;!-- SC_ON --&gt;',
    'likes': null,
    'suggested_sort': null,
    'banned_at_utc': null,
    'view_count': null,
    'archived': false,
    'no_follow': false,
    'is_crosspostable': false,
    'pinned': false,
    'over_18': false,
    'all_awardings': [

    ],
    'media': null,
    'media_only': false,
    'can_gild': false,
    'spoiler': false,
    'locked': false,
    'author_flair_text': null,
    'visited': false,
    'num_reports': null,
    'distinguished': null,
    'subreddit_id': 't5_38jhw',
    'mod_reason_by': null,
    'removal_reason': null,
    'link_flair_background_color': '',
    'id': 'c1yuig',
    'is_robot_indexable': true,
    'report_reasons': null,
    'author': 'noilddude',
    'num_crossposts': 0,
    'num_comments': 19,
    'send_replies': true,
    'contest_mode': false,
    'author_patreon_flair': false,
    'author_flair_text_color': null,
    'permalink': '/r/vuejs/comments/c1yuig/mevn_stack_app_should_i_be_making_api_requests/',
    'whitelist_status': null,
    'stickied': false,
    'url': 'https://www.reddit.com/r/vuejs/comments/c1yuig/mevn_stack_app_should_i_be_making_api_requests/',
    'subreddit_subscribers': 28960,
    'created_utc': 1560839973.0,
    'mod_reports': [

    ],
    'is_video': false
  }
]
