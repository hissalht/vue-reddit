import postsFixture from './posts'

export default postsFixture.map(post => ({
  id: post.name,
  title: post.title,
  score: post.score,
  subreddit: post.subreddit,
  url: post.url,
  textHtml: post.selftext_html,
  isText: post.is_self
}))
