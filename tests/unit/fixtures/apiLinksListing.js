import postsFixture from './posts'

export default {
  'kind': 'Listing',
  'data': {
    'modhash': '',
    'dist': 2,
    'children': [
      {
        'kind': 't3',
        'data': postsFixture[0]
      },
      {
        'kind': 't3',
        'data': postsFixture[1]
      },
      {
        'kind': 't3',
        'data': postsFixture[2]
      }
    ],
    'after': 't3_bzhy6x',
    'before': null
  }
}
