import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

import App from '@/App'
import SubFinder from '@/components/SubFinder'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('App', () => {
  const build = () => {
    const wrapper = shallowMount(App, {
      localVue
    })
    return {
      wrapper,
      heading: () => wrapper.find('h1'),
      finder: () => wrapper.find(SubFinder)
    }
  }
  it('renders the component', () => {
    const { wrapper } = build()
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('renders the heading of the app', () => {
    const { heading } = build()
    expect(heading().exists()).toBe(true)
    expect(heading().text()).toContain('Vue Reddit')
  })

  it('renders the sub finder', () => {
    const { finder } = build()
    expect(finder().exists()).toBe(true)
  })
})
