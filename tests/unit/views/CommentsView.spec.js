import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import flushPromises from 'flush-promises'

import CommentsView from '@/views/CommentsView'
import CommentsTree from '@/components/CommentsTree'
import * as actions from '@/store/actions'
import { getInitialState } from '@/store/state'
import storeFixture from '../fixtures/store'

jest.mock('@/store/actions')

const localVue = createLocalVue()
localVue.use(Vuex)

describe('CommentsView', () => {
  let sub
  let link
  let state
  let push

  const build = () => {
    const wrapper = shallowMount(CommentsView, {
      localVue,
      store: new Vuex.Store({ actions, state }),
      mocks: {
        $route: {
          params: {
            sub,
            id: link,
            slug: 'slug'
          }
        },
        $router: {
          push
        }
      }
    })
    return {
      wrapper,
      commentsTree: () => wrapper.find(CommentsTree),
      refresh: () => wrapper.find('button.refresh'),
      commentsPlaceholder: () => wrapper.find('.comments-placeholder'),
      op: () => wrapper.find('.original-post'),
      back: () => wrapper.find('button.back')
    }
  }

  beforeEach(() => {
    jest.resetAllMocks()
    sub = 'vuejs'
    link = 'abcdef'
    state = getInitialState()
    push = jest.fn()
  })

  it('renders the component', () => {
    const { wrapper } = build()
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('fetches the comments', async () => {
    const { wrapper } = build()
    expect(actions.FETCH_COMMENTS).toHaveBeenCalled()
    expect(actions.FETCH_COMMENTS.mock.calls[0][1]).toEqual({
      sub: 'vuejs',
      link: 'abcdef'
    })
    expect(wrapper.vm.loading).toEqual(true)
    await flushPromises()
    expect(wrapper.vm.loading).toEqual(false)
  })

  it('doesnt fetch comments if already fetched', () => {
    state = JSON.parse(JSON.stringify(storeFixture))
    link = 'linkab'
    build()
    expect(actions.FETCH_COMMENTS).not.toHaveBeenCalled()
  })

  it('binds the relevant comments from the state', () => {
    state = storeFixture
    link = 'linkab'
    const { wrapper } = build()
    expect(wrapper.vm.comments).toBeDefined()
    expect(wrapper.vm.comments).toHaveLength(2)
  })

  it('binds empty array comments if the relevant linkIndex doesnt exist', () => {
    state = storeFixture
    link = 'unknw'
    const { wrapper } = build()
    expect(wrapper.vm.comments).toEqual([])
  })

  it('binds the relevent link from the state', () => {
    link = 'c0357x'
    state = storeFixture
    const { wrapper } = build()
    expect(wrapper.vm.link).toBeDefined()
    expect(wrapper.vm.link).toEqual(storeFixture.links.entities['t3_' + link])
  })

  it('renders a CommentsTree with the correct props', () => {
    state = storeFixture
    link = 'linkab'
    const { wrapper, commentsTree } = build()
    expect(commentsTree().exists()).toBe(true)
    expect(commentsTree().vm.comments).toBe(wrapper.vm.comments)
  })

  it('renders a refresh button that fetches comments when clicked', async () => {
    const { refresh } = build()
    expect(refresh().exists()).toBe(true)

    await flushPromises()
    jest.resetAllMocks()
    refresh().trigger('click')

    expect(actions.FETCH_COMMENTS).toHaveBeenCalled()
    expect(actions.FETCH_COMMENTS.mock.calls[0][1]).toEqual({
      sub: 'vuejs',
      link: 'abcdef'
    })
  })

  it('the refresh button is disabled while the comments are loading', () => {
    const { refresh, wrapper } = build()
    wrapper.setData({ loading: true })
    expect(refresh().element.getAttribute('disabled')).toBeTruthy()
    wrapper.setData({ loading: false })
    expect(refresh().element.getAttribute('disabled')).toBeFalsy()
  })

  it('a placeholder is rendered in place of the comments when the comments are loading', () => {
    const { commentsPlaceholder, wrapper, commentsTree } = build()
    wrapper.setData({ loading: true })
    expect(commentsPlaceholder().exists()).toBe(true)
    expect(commentsTree().exists()).toBe(false)
    wrapper.setData({ loading: false })
    expect(commentsPlaceholder().exists()).toBe(false)
    expect(commentsTree().exists()).toBe(true)
  })

  it('renders the original post url', async () => {
    state = storeFixture
    link = 'c0357x'
    const { op } = build()
    await flushPromises()

    expect(op().exists()).toBe(true)
    const url = op().find('a.url')
    expect(url.exists()).toBe(true)
    expect(url.text()).toContain(state.links.entities['t3_' + link].url)
  })

  it('renders the op self-text', async () => {
    state = storeFixture
    link = 'c1yuig'
    const { op } = build()
    await flushPromises()
    const opText = op().find('.self-text')
    expect(opText.exists()).toBe(true)
    expect(opText.element.innerHTML).toEqual(state.links.entities['t3_' + link].textHtml)
  })

  it('renders a back button', () => {
    const { back } = build()
    expect(back().exists()).toBe(true)

    back().trigger('click')
    expect(push).toHaveBeenCalledWith({ name: 'posts', params: { sub } })
  })
})
