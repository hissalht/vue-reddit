import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import flushPromises from 'flush-promises'

import PostsView from '@/views/PostsView'
import PostList from '@/components/PostList'
import { getInitialState } from '@/store/state'
import * as actions from '@/store/actions'
import storeFixture from '../fixtures/store'

jest.mock('@/store/actions')

const localVue = createLocalVue()
localVue.use(Vuex)

describe('PostsView', () => {
  let state
  let currentSub

  const build = () => {
    const wrapper = shallowMount(PostsView, {
      localVue,
      store: new Vuex.Store({ state, actions }),
      mocks: {
        $route: {
          params: {
            sub: currentSub
          }
        }
      },
      data: () => ({
        loading: false
      })
    })
    return {
      wrapper,
      list: () => wrapper.find(PostList),
      subName: () => wrapper.find('.subreddit-name'),
      placeholder: () => wrapper.find('.placeholder'),
      refresh: () => wrapper.find('button.refresh')
    }
  }

  beforeEach(() => {
    jest.resetAllMocks()
    state = getInitialState()
    currentSub = 'gifs'
  })

  it('renders the component', () => {
    state = JSON.parse(JSON.stringify(storeFixture))
    const { wrapper } = build()
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('renders a placeholder instead of post list only when property "loading" is true', async () => {
    const { wrapper, placeholder } = build()
    wrapper.setData({ loading: true })
    expect(placeholder().exists()).toBe(true)
    wrapper.setData({ loading: false })
    expect(placeholder().exists()).toBe(false)
  })

  it('renders the name of the sub', () => {
    currentSub = 'vuejs'
    const { subName } = build()
    expect(subName().exists()).toBe(true)
    expect(subName().text()).toEqual(`r/${currentSub}`)
  })

  it('render a PostList and pass it binded data posts', () => {
    state = JSON.parse(JSON.stringify(storeFixture))
    currentSub = 'gifs'
    const { list } = build()
    expect(list().exists()).toBe(true)
    expect(list().vm.posts).toEqual(
      storeFixture.links.subredditIndex.gifs.map(id => storeFixture.links.entities[id])
    )
  })

  it('fetches the posts if not already done', async () => {
    currentSub = 'gifs'
    const { wrapper } = build()
    expect(actions.FETCH_LINKS).toHaveBeenCalled()
    expect(actions.FETCH_LINKS.mock.calls[0][1]).toEqual({
      sub: 'gifs'
    })
    expect(wrapper.vm.loading).toEqual(true)
    await flushPromises()
    expect(wrapper.vm.loading).toEqual(false)
  })

  it('doesnt automatically fetch posts if already fetched', () => {
    currentSub = 'gifs'
    state = JSON.parse(JSON.stringify(storeFixture))
    build()
    expect(actions.FETCH_LINKS).not.toHaveBeenCalled()
  })

  it('renders a refresh button', () => {
    const { wrapper, refresh } = build()
    expect(refresh().exists()).toBe(true)
    wrapper.setData({ loading: true })
    expect(refresh().element.getAttribute('disabled')).toBeTruthy()
    wrapper.setData({ loading: false })
    expect(refresh().element.getAttribute('disabled')).toBeFalsy()
  })

  it('fetches the links when clicking the refresh button', async () => {
    const { refresh } = build()
    await flushPromises() // flush the initial fetch (the refresh button is disabled while loading)

    jest.resetAllMocks()
    refresh().trigger('click')

    expect(actions.FETCH_LINKS).toHaveBeenCalled()
    expect(actions.FETCH_LINKS.mock.calls[0][1]).toEqual({ sub: currentSub, invalidate: true })
  })

  it('fetches the link when the current sub changes', async () => {
    currentSub = 'gifs'
    const newSub = 'vuejs'
    const { wrapper } = build()
    await flushPromises()
    jest.resetAllMocks()

    wrapper.vm.$route.params = {
      sub: newSub
    }

    expect(actions.FETCH_LINKS).toHaveBeenCalled()
    expect(actions.FETCH_LINKS.mock.calls[0][1]).toEqual({ sub: newSub })
  })
})
