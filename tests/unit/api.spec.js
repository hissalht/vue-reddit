import flushPromises from 'flush-promises'
import nock from 'nock'

import * as api from '@/api'
import listingFixture from './fixtures/apiLinksListing'
import normalizedPostsFixture from './fixtures/normalizedPosts'

import apiLinksResponse from './fixtures/api/links.json'
import apiCommentsResponse from './fixtures/api/comments'

const API_URL = 'https://api.reddit.com'

describe('api', () => {
  describe('fetchPosts', () => {
    it('fetches the posts', async () => {
      const sub = 'vuejs'
      const request = nock(API_URL)
        .get(`/r/${sub}`)
        .query({ raw_json: 1 })
        .reply(200, listingFixture)

      const result = await api.fetchPosts(sub)
      await flushPromises()

      expect(result.after).toEqual(listingFixture.data.after)
      expect(result.posts).toEqual(normalizedPostsFixture)
      expect(request.isDone()).toBe(true)
    })

    it('fetches the next page of posts', async () => {
      const sub = 'vuejs'
      const request = nock(API_URL)
        .get(`/r/${sub}`)
        .query({ after: 't3_after', raw_json: 1 })
        .reply(200, listingFixture)
      await api.fetchPosts(sub, 't3_after')
      await flushPromises()

      expect(request.isDone()).toBe(true)
    })
  })

  describe('normalizeListing', () => {
    it('returns an object of the correct format', () => {
      const response = {}
      const result = api.normalizeListing(response)
      expect(result).toHaveProperty('comments')
      expect(result).toHaveProperty('links')
    })

    it('normalize links api response', () => {
      const response = apiLinksResponse
      const result = api.normalizeListing(response)
      expect(result.links).toHaveLength(3)
    })

    it('normalize comments api response', () => {
      const data = apiCommentsResponse[1]
      const result = api.normalizeListing(data)
      expect(result.comments).toHaveLength(25)
    })
  })

  describe('normalizeResponse', () => {
    it('returns an object of the correct format', () => {
      const response = {}
      const result = api.normalizeResponse(response)
      expect(result).toHaveProperty('comments')
      expect(result).toHaveProperty('links')
    })

    it('normalize api listing response', () => {
      const response = apiCommentsResponse
      const result = api.normalizeResponse(response)
      expect(result.comments).toHaveLength(25)
      expect(result.links).toHaveLength(1)
    })
  })

  describe('fetchComments', () => {
    it('fetches the comments', async () => {
      const sub = 'vuejs'
      const linkId = 't3_abcdef'
      const request = nock(API_URL)
        .get(`/r/${sub}/comments/${linkId.slice(3)}`)
        .query({ raw_json: 1 })
        .reply(200, apiCommentsResponse)

      const result = await api.fetchComments(sub, linkId)
      await flushPromises()

      expect(result.links).toHaveLength(1)
      expect(result.comments).toHaveLength(25)
      expect(request.isDone()).toBe(true)
    })
  })
})
