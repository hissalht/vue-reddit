import Vue from 'vue'
import Vuex from 'vuex'

import { getInitialState } from './state'
import * as mutations from './mutations'
import * as actions from './actions'

Vue.use(Vuex)

// const localStorageStateKey = 'v-reddit:store'

// function saveToLocalStorage (state) {
//   localStorage.setItem(localStorageStateKey, JSON.stringify(state))
// }

// function loadLocalStorage () {
//   const json = localStorage.getItem(localStorageStateKey)
//   try {
//     return JSON.parse(json)
//   } catch {
//     return {}
//   }
// }

const store = new Vuex.Store({
  state: {
    ...getInitialState()
    // ...loadLocalStorage()
  },
  mutations,
  actions
})

// store.subscribe((mutation, state) => {
//   // saveToLocalStorage(state)
// })

export default store
