import Vue from 'vue'

function deepCopy (obj) {
  return JSON.parse(JSON.stringify(obj))
}

export const SET_LINKS = (state, { links, invalidate }) => {
  if (invalidate) {
    Vue.set(state.links, 'subredditIndex', {})
  }
  for (let link of links) {
    const sub = link.subreddit.toLowerCase()
    Vue.set(state.links.entities, link.id, deepCopy(link))
    if (!state.links.subredditIndex[sub]) {
      Vue.set(state.links.subredditIndex, sub, [])
    }
    if (state.links.subredditIndex[sub].indexOf(link.id) === -1) {
      state.links.subredditIndex[sub].push(link.id)
    }
  }
}

export const SET_AFTER = (state, { sub, after }) => {
  Vue.set(state.links.afters, sub, after)
}

export const SET_COMMENTS = (state, { comments, invalidate }) => {
  if (invalidate) {
    Vue.set(state.comments, 'linkIndex', {})
  }
  for (let comment of comments) {
    Vue.set(state.comments.entities, comment.id, deepCopy(comment))
    if (!state.comments.linkIndex[comment.link]) {
      Vue.set(state.comments.linkIndex, comment.link, [])
    }
    if (comment.depth === 0 && state.comments.linkIndex[comment.link].indexOf(comment.id) === -1) {
      state.comments.linkIndex[comment.link].push(comment.id)
    }
  }
}

export const HIDE_COMMENT = (state, { id }) => {
  Vue.set(state.comments.entities[id], 'hidden', true)
}

export const SHOW_COMMENT = (state, { id }) => {
  Vue.set(state.comments.entities[id], 'hidden', false)
}
