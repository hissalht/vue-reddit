export const FETCH_LINKS = jest.fn().mockResolvedValue()
export const FETCH_COMMENTS = jest.fn().mockResolvedValue()
export const TOGGLE_HIDDEN_COMMENT = jest.fn()
