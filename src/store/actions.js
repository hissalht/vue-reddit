import { fetchPosts, fetchComments } from '@/api'

export const FETCH_LINKS = ({ commit }, { sub, invalidate }) => {
  return fetchPosts(sub)
    .then(({ posts, after }) => {
      commit('SET_LINKS', { links: posts, invalidate })
      commit('SET_AFTER', { sub, after })
    })
}

export const FETCH_COMMENTS = ({ commit }, { sub, link }) => {
  return fetchComments(sub, link)
    .then(({ comments, links }) => {
      commit('SET_COMMENTS', { comments })
      commit('SET_LINKS', { links })
    })
}

export const TOGGLE_HIDDEN_COMMENT = ({ commit, state }, { id }) => {
  if (state.comments.entities[id].hidden) {
    commit('SHOW_COMMENT', { id })
  } else {
    commit('HIDE_COMMENT', { id })
  }
}
