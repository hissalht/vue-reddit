export const getInitialState = () => ({
  comments: {
    entities: {}, // fullname => comment
    linkIndex: {} // linkId => [commentIds], only the first level comments
  },
  links: {
    entities: {}, // fullname => link
    subredditIndex: {}, // subreddit => [linkIds]
    afters: {} // sub => after
  }
})
