
export default [
  {
    name: 'posts',
    path: '/r/:sub',
    component: () => import('@/views/PostsView')
  },
  {
    name: 'comments',
    path: '/r/:sub/comments/:id/:slug',
    component: () => import('@/views/CommentsView')
  }
]
