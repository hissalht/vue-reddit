import axios from 'axios'
import httpAdapter from 'axios/lib/adapters/http'

const API_URL = 'https://api.reddit.com'

const instance = axios.create({
  baseURL: API_URL,
  adapter: httpAdapter
})

instance.interceptors.request.use(config => {
  if (!config.params) {
    config.params = {}
  }
  config.params['raw_json'] = 1
  return config
})

export const Types = {
  Comment: 't1',
  Account: 't2',
  Link: 't3',
  Message: 't4',
  Subreddit: 't5',
  Award: 't6',
  Listing: 'Listing'
}

function normalizeLink (link) {
  return {
    id: link.name,
    title: link.title,
    score: link.score,
    subreddit: link.subreddit,
    url: link.url,
    textHtml: link.selftext_html,
    isText: link.is_self
  }
}

function normalizeComment (comment) {
  const result = []
  const normalizedComment = {
    id: comment.name,
    body: comment.body,
    bodyHtml: comment.body_html,
    score: comment.score,
    link: comment.link_id,
    author: comment.author,
    depth: comment.depth,
    replies: []
  }
  result.push(normalizedComment)
  if (comment.replies) {
    // recursively go through every children comments
    for (let reply of comment.replies.data.children) {
      const children = normalizeComment(reply.data)
      normalizedComment.replies.push(reply.data.name)
      result.push(...children)
    }
  }
  return result
}

export function normalizeListing ({ data }) {
  const results = {
    links: [],
    comments: []
  }
  if (!data) {
    return results
  }
  for (let child of data.children) {
    switch (child.kind) {
      case Types.Link:
        results.links.push(normalizeLink(child.data))
        break
      case Types.Comment:
        results.comments.push(...normalizeComment(child.data))
        break
    }
  }
  return results
}

export function normalizeResponse (data) {
  const results = {
    links: [],
    comments: []
  }

  // make sure every responses are treated the same
  if (!Array.isArray(data)) {
    data = [data]
  }

  for (let dataItem of data) {
    switch (dataItem.kind) {
      case Types.Listing:
        const listingResult = normalizeListing(dataItem)
        results.links.push(...listingResult.links)
        results.comments.push(...listingResult.comments)
        break
    }
  }

  return results
}

export function fetchPosts (sub, after) {
  return instance.get(`/r/${sub}`, {
    params: {
      ...(after && { after })
    }
  })
    .then(response => response.data.data)
    .then(data => ({
      after: data.after,
      posts: data.children.map(({ data }) => normalizeLink(data))
    }))
}

export function fetchComments (sub, linkName) {
  let id = linkName
  if (linkName.indexOf('t3_') === 0) {
    id = linkName.slice(3)
  }
  const url = `/r/${sub}/comments/${id}`
  return instance.get(url)
    .then(response => response.data)
    .then(response => normalizeResponse(response))
}
