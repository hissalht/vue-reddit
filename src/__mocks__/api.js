import postsFixtures from '../../tests/unit/fixtures/normalizedPosts'
import commentsFixture from '../../tests/unit/fixtures/normalizedComments'

export const fetchPosts = jest.fn().mockResolvedValue({
  posts: postsFixtures,
  after: 't3_mocked'
})

export const fetchComments = jest.fn().mockResolvedValue({
  comments: commentsFixture,
  links: [postsFixtures[0]]
})
